<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\AgiLogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [VehicleController::class, 'index'])->name('vehicles.index'); 
Route::get('vehicles/log-count/{id}', [AgiLogController::class, 'index'])->name('agi-log.count'); 
Route::get('vehicles/last-info/{id}', [AgiLogController::class, 'getLastInfo'])->name('agi-log.last.info'); 
