<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class AgiLogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ini_set('memory_limit', '-1');
        $path 	= database_path('seeders/sql/agi_logs.sql');
        
        $sql 	= file_get_contents($path);
        
        $statements = array_filter(array_map('trim', explode(';', $sql)));
        foreach ($statements as $stmt) {
            \DB::statement($stmt);
        }
    }
}
