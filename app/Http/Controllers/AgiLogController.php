<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vehicle;
use App\Models\AgiLog;
use DB;
use DataTables;

class AgiLogController extends Controller
{
    /**
     * Display a listing of the Agi Log Count from resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id)
    {
        $vehicle = Vehicle::findOrfail($id);
        if ($request->ajax()) {
            $agiLog = AgiLog::select('local_time',DB::raw("COUNT(*) as log_count"),DB::raw("DATE_FORMAT(local_time, '%Y-%m') local_time"))
                          ->where('vehicle_id', $id)
                          ->groupBy(DB::raw('YEAR(local_time)'),DB::raw('MONTH(local_time)'))
                          ->orderby('local_time', 'DESC')->get();

                return Datatables::of($agiLog)
                    ->addIndexColumn()
                    ->addColumn('vehicle.name',function($raw) use($vehicle){
                        return $vehicle->name ?? '';
                    })
                    ->addColumn('log_count',function($raw){
                        return isset($raw->log_count) ? number_format($raw->log_count) : '';
                    })
                    ->rawColumns(['local_time','vehicle.name'])->make(true);

        }
        return view('agi-logs.index', compact('id'));
    }
    

    /**
     * Display last information from the Agi Log resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getLastInfo($id){
        $vehicle = Vehicle::findOrfail($id);
        $agiLog =AgiLog::where('vehicle_id', $id)->orderby('local_time', 'DESC')->first();
        $locationAddress = $this->getLocationFromVirtualearth($agiLog->lat, $agiLog->lng);
        return view('agi-logs.last-info', compact('agiLog', 'locationAddress'));
    }


    /**
     * Get the address from virtualearth api.
     *
     * @return \Illuminate\Http\Response
     */
    private function getLocationFromVirtualearth($lat,$lng)
    {
        // virtualearth api key
        $key = env('BING_API',"ArYNVja8zEWlCU3xBYr-y1CYJ0zVEUBNV7DuureCK-b3L_W8PJMAzfawjaCChHAp");
        $endpoint = 'https://dev.virtualearth.net/REST/v1/Locations/'.$lat.','.$lng.'?o=xml&key='.$key;

        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $endpoint);

        $addressName = '';
        if($response->getStatusCode() == 200){
            $locationDetails = simplexml_load_string($response->getBody());
            $addressName = $locationDetails->ResourceSets->ResourceSet->Resources->Location->Name;
        }
        return $addressName;
    }
}
