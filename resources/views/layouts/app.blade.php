<!DOCTYPE html>
<html lang="en">
<head>
    <title>Netcorp</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="{{ trans('panel.site_title') }}" />
    <meta name="keywords" content="{{ trans('panel.site_title') }}">
    <meta name="author" content="Codedthemes" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- styles -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" rel="stylesheet">

    @yield('styles') 
    
    
</head>
<body>
     <div class="auth-wrapper">
        <div class="auth-content subscribe">
            @if(session('message'))
                <div class="row mb-2">
                    <div class="col-lg-12">
                        <div class="alert alert-success m-4" role="alert">{{ session('message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                              <span class="" aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>                
            @endif
            <div class="pcoded-content">
                <div class="pcoded-inner-content">
                    <!-- [ breadcrumb ] start -->

                    <!-- [ breadcrumb ] end -->
                    <div class="main-body">
                        <div class="page-wrapper">
                            <div class="p-3">
                            @yield('content')
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
</body>
</html>

<!-- Scripts  -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js"></script>

@yield('scripts')
    
   

