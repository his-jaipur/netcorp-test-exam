@extends('layouts.app')

@section('styles')
    <style>
        #map {
            height: 400px;
            width: 100%;
        }
    </style>
@endsection

@section('content')
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h3>Last Info</h3>
        </div>
    </div>
    <table class="table table-hover table-bordered text-center">
        <thead class="bg-secondary text-white" >
            <tr>
                <th width="80px">ID</th>
                <th>Name</th>
                <th>Local Time</th>
                <th>latitude</th>
                <th>Longitude</th>
                <th>Location</th>
                <th>Speed</th>
                <th>Direction</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="80px">{{ $agiLog->vehicle->id }}</td>
                <td>{{ $agiLog->vehicle->name }}</td>
                <td>{{ $agiLog->local_time }}</td>
                <td>{{ $agiLog->lat }}</td>
                <td>{{ $agiLog->lng }}</td>
                <td>{{ $locationAddress }}</td>
                <td>{{ $agiLog->speed }}</td>
                <td>{{ $agiLog->direction }}</td>
            </tr>
        </tbody>
    </table>

    <div id="map"></div>
@endsection

@section('scripts')
    <script
        src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAP_API','AIzaSyCDrJ7VKizx6cWEEbwcj7CbapTVDLQbuC4')}}&callback=initMap&libraries=&v=weekly"
        async
    ></script>
    <script>
        // Initialize and add the map
        function initMap() {
            const uluru = { lat: {{ $agiLog->lat }}, lng: {{ $agiLog->lng }} };
            // The map, centered at Uluru
            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 10,
                center: uluru,
            });

            const contentString =
            '<div id="content">' +
            '<h6 id="firstHeading" class="firstHeading"> {{ $locationAddress }}</h6>' +
            "</div>";

            const infowindow = new google.maps.InfoWindow({
                content: contentString,
            });

            // The marker, positioned at Uluru
            const marker = new google.maps.Marker({
                position: uluru,
                map: map,
            });

            marker.addListener('mouseover', function() {
                infowindow.open(map, this);
            });

            // assuming you also want to hide the infowindow when user mouses-out
            marker.addListener('mouseout', function() {
                infowindow.close();
            });
        }
    </script>
@endsection